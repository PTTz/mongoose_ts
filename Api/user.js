const express = require('express');
const mongoose = require('mongoose');
const User = require('../db/user');
const route = express.Router();

route.post('/',async(req,res)=>{
    const{name,score} = req.body;
    let user = {};
    user.name = name;
    user.score = score;
    let userModel = new User(user);
    await userModel.save();
    res.json(userModel);

    console.log('user log');
})

module.exports = route;