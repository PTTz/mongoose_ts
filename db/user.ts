import { Model, model, Schema, Document } from "mongoose";
export const RankSchema = new Schema({
    rank : {type : Number , required : true},
    name : {type : String , require : true},
    score : {type : Number , required : true}
})
const Rank = model('user', RankSchema);

interface IUser{
    rank : number;
    name : string;
    score : number;
}
interface IUserDocs extends IUser, Document
{
    normalMethod() : Promise<string>;
}
interface IUserModel extends Model<IUserDocs>{
    staticMethod() : number;
}
const schema = new Schema<IUserDocs,IUserModel>({
    rank :{type : Number,required : true,unique : true,},
    name :{type : String,required : true,unique : true,},
    score :{type : Number,required : true,unique : true,},
})
schema.statics.staticMethod = function () {
    return Math.random();
  };
schema.methods.normalMethod = async function () {
    const str = "A Normal String";
    return str;
  };
export const User = model("User", schema);
export class RankController {
    async add(){
        console.log('call add in database');
        const newTest = new User({ rank : 2 , name : 'empty' , score : 123 });
        await newTest.save();
        // var data = await Rank.find({});
        // console.log(data);
    }

}
export default RankController;

