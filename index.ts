import { Room, Client } from "colyseus";
import { Schema, MapSchema, type } from "@colyseus/schema";
import  User  from "./db/user";

// Our custom game state, an ArraySchema of type Player only at the moment
export class State extends Schema {

}

interface IMessage
{
  rank : number;
  name : string;
  score : number;
}

export class RankTable extends Room<State> {
  maxClients = 9999999;
  onCreate() {
    console.log("client is joining");
    this.onMessage("move", (client, data) => {
      console.log(client.id+' have joined');
    });
    this.messagehere();
  }
  onJoin(client: Client, options: any) {
    console.log(' client '+client.id+" joined ");
  }
  messagehere(){
    this.onMessage<IMessage>('c2s_NewHightScore',(client,obj)=>{
      console.log('message c2s_NewHightScore');
      var x = new User();
      x.add();
      // x.find();
    })
  }
}